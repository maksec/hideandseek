using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using TMPro;

public class PlayerData : MonoBehaviourPunCallbacks
{
    public enum PlayerRole
    {
        Seeker,
        Red,
        Green,
        Blue,
        None
    }

    public static PlayerRole player_role = PlayerRole.None;
    public static int round_time_seconds = 60;
    public static bool is_room = true;

    public void SetPlayerRole(PlayerRole role)
    {
        player_role = role;
    }

    public PlayerRole GetPlayerRole()
    {
        return player_role;
    }

    public void SetRoundTime(int round_time)
    {
        round_time_seconds = round_time;
    }

    public int GetRoundTime()
    {
        return round_time_seconds;
    }

    public bool GetIsRoom()
    {
        return is_room;
    }

    public void SetIsRoom(bool room) {
        is_room = room;
    }
}
