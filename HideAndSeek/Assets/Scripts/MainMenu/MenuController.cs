using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;
using WebSocketSharp;
using System.Security.Cryptography;
using UnityEngine.UIElements;
using System.Data.SqlTypes;

public class MenuManager : MonoBehaviourPunCallbacks
{
    public GameObject panel_loading;
    public GameObject panel_main_menu;
    public GameObject panel_change_name;
    public GameObject panel_quit;
    public GameObject panel_play;
    public GameObject panel_create_lobby;
    public GameObject panel_join_lobby;
    public GameObject panel_empty_lobby_name_create;
    public GameObject panel_empty_lobby_name_join;
    public GameObject panel_lobby_name_used;
    public GameObject panel_no_such_lobby;
    public GameObject panel_wrong_password;
    public GameObject panel_settings;

    public UnityEngine.UI.Toggle full_screen_toggle;
    public TMP_Dropdown resolution_dropdown;
    public TMP_Dropdown quality_dropdown;

    private GameObject start_button;

    public TMP_InputField create_lobby_name;
    public TMP_InputField join_lobby_name;
    public TMP_InputField create_lobby_password;
    public TMP_InputField join_lobby_password;

    public TMP_InputField nickname;

    const string kMainMenu = "main_menu";
    const string kChangeName = "change_name";
    const string kQuit = "quit";
    const string kLoading = "loading";
    const string kPlay = "play";
    const string kCreateLobby = "create_lobby";
    const string kJoinLobby = "join_lobby";
    const string kEmptyLobbyNameCreate = "empty_lobby_name_create";
    const string kEmptyLobbyNameJoin = "empty_lobby_name_join";
    const string kLobbyNameUsed = "lobby_name_used";
    const string kNoSuchLobby = "no_such_lobby";
    const string kWrongPassword = "wrong_password";
    const string kSettings = "settings";

    Resolution[] resolutions;

    private void Start()
    {
        Screen.fullScreen = false;
        full_screen_toggle.isOn = false;
        resolution_dropdown.ClearOptions();
        List<string> options = new List<string>();
        resolutions = Screen.resolutions;
        int current_index = 0;

        for(int i=0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height + " " + resolutions[i].refreshRateRatio + "hz";
            options.Add(option);
            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                current_index = i;
            }
        }
        resolution_dropdown.AddOptions(options);
        resolution_dropdown.RefreshShownValue();

        resolution_dropdown.value = current_index;

        SetActivePanel(kLoading);

        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public void setFullScreen()
    {
        Screen.fullScreen = full_screen_toggle.isOn;
    }

    public void setResolution()
    {
        Screen.SetResolution(resolutions[resolution_dropdown.value].width, resolutions[resolution_dropdown.value].height, full_screen_toggle.isOn);
    }

    public void setQuality()
    {
        QualitySettings.SetQualityLevel(quality_dropdown.value);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.NickName = "Player";
        nickname.text = "Player";

        SetActivePanel(kMainMenu);
        Debug.Log("Connected to Master");
    }

    public void CreateRoom()
    {
        SetActivePanel(kLoading);

        if (create_lobby_name.text.IsNullOrEmpty())
        {
            SetActivePanel(kEmptyLobbyNameCreate);
            return;
        }

        ExitGames.Client.Photon.Hashtable roomProperties = new ExitGames.Client.Photon.Hashtable
        {
            { "Password", ""}
        };

        if (!create_lobby_password.text.IsNullOrEmpty())
        {
            roomProperties["Password"] = create_lobby_password.text;
        }

        RoomOptions room_options = new RoomOptions();
        room_options.MaxPlayers = 4;
        room_options.CustomRoomProperties = roomProperties;

        PhotonNetwork.CreateRoom(create_lobby_name.text, room_options);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);

        Debug.Log("Failed to create lobby. Return code: " + returnCode + ", message: " + message);
        if (message == "A game with the specified id already exist.")
        {
            SetActivePanel(kLobbyNameUsed);
        }
    }

    public void JoinRoom()
    {
        SetActivePanel(kLoading);

        if (join_lobby_name.text.IsNullOrEmpty())
        {
            SetActivePanel(kEmptyLobbyNameJoin);
            return;
        }

        PhotonNetwork.JoinRoom(join_lobby_name.text);
    }

    public override void OnJoinedRoom()
    {
        /*Debug.Log(PhotonNetwork.IsMasterClient + " " + join_lobby_password.text + " " + PhotonNetwork.CurrentRoom.CustomProperties["Password"].ToString());
        if (!PhotonNetwork.IsMasterClient && join_lobby_password.text != PhotonNetwork.CurrentRoom.CustomProperties["Password"].ToString())
        {
            LeaveRoom();
            SetActivePanel(kWrongPassword);
            return;
        }*/

        PhotonNetwork.LoadLevel("Lobby");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);

        Debug.Log("Failed to join lobby. Return code: " + returnCode + ", message: " + message);
        if (message == "Game does not exist")
        {
            SetActivePanel(kNoSuchLobby);
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        start_button.SetActive(PhotonNetwork.IsMasterClient);
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();

            SetActivePanel(kLoading);
        }
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        Debug.Log("Leaving lobby");
    }

    public void SetActivePanel(string panel_name)
    {
        panel_main_menu.SetActive(false);
        panel_change_name.SetActive(false);
        panel_quit.SetActive(false);
        panel_loading.SetActive(false);
        panel_play.SetActive(false);
        panel_create_lobby.SetActive(false);
        panel_join_lobby.SetActive(false);
        panel_empty_lobby_name_create.SetActive(false);
        panel_empty_lobby_name_join.SetActive(false);
        panel_lobby_name_used.SetActive(false);
        panel_no_such_lobby.SetActive(false);
        panel_wrong_password.SetActive(false);
        panel_settings.SetActive(false);

        switch (panel_name)
        {
            case kMainMenu:
                panel_main_menu.SetActive(true);
                Debug.Log("MainMenu");
                break;
            case kChangeName:
                panel_change_name.SetActive(true);
                Debug.Log("ChangeName");
                break;
            case kQuit:
                panel_quit.SetActive(true);
                Debug.Log("Quit");
                break;
            case kLoading:
                panel_loading.SetActive(true);
                Debug.Log("Loading");
                break;
            case kPlay:
                panel_play.SetActive(true);
                Debug.Log("Play");
                break;
            case kCreateLobby:
                panel_create_lobby.SetActive(true);
                Debug.Log("CreateLobby");
                break;
            case kJoinLobby:
                panel_join_lobby.SetActive(true);
                Debug.Log("JoinLobby");
                break;
            case kEmptyLobbyNameCreate:
                panel_empty_lobby_name_create.SetActive(true);
                Debug.Log("EmptyLobbyNameCreate");
                break;
            case kEmptyLobbyNameJoin:
                panel_empty_lobby_name_join.SetActive(true);
                Debug.Log("EmptyLobbyNameJoin");
                break;
            case kLobbyNameUsed:
                panel_lobby_name_used.SetActive(true);
                Debug.Log("LobbyNameUsed");
                break;
            case kNoSuchLobby:
                panel_no_such_lobby.SetActive(true);
                Debug.Log("NoSuchLobby");
                break;
            case kWrongPassword:
                panel_wrong_password.SetActive(true);
                Debug.Log("WrongPassword");
                break;
            case kSettings:
                panel_settings.SetActive(true);
                Debug.Log("Settings");
                break;
            default: break;
        }
    }

    public void SaveName()
    {
        PhotonNetwork.LocalPlayer.NickName = nickname.text;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
