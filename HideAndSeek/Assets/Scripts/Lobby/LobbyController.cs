using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;
using WebSocketSharp;
using System.Security.Cryptography;
using System.Linq;
using Unity.Properties;

[RequireComponent(typeof(PlayerData))]

public class LobbyController : MonoBehaviourPunCallbacks
{

    public TMP_Text RoomNameText;
    public TMP_Text CurPlText;

    public GameObject StartButton;
    public GameObject GenerateRoomsPanel;
    public Toggle GenerateRoomToggle;

    public GameObject MenuUi;
    public GameObject LobbyInfo;
    public GameObject NickNamePanel;

    public GameObject SeekerChooseBtn;
    public GameObject RedChooseBtn;
    public GameObject GreenChooseBtn;
    public GameObject BlueChooseBtn;

    public GameObject SeekerLeaveBtn;
    public GameObject RedLeaveBtn;
    public GameObject GreenLeaveBtn;
    public GameObject BlueLeaveBtn;

    public GameObject seeker_spawn_position;
    public GameObject[] hiders_spawn_positions;

    public TMP_Text SeekerNick;
    public TMP_Text RedNick;
    public TMP_Text GreenNick;
    public TMP_Text BlueNick;

    private GameObject player_model;

    private bool seeker_available = true;
    private bool red_available = true;
    private bool green_available = true;
    private bool blue_available = true;

    private PlayerData player_data;

    [PunRPC]
    public void BlockSeeker(string name)
    {
        seeker_available = false;
        SeekerNick.text = name;
        SeekerChooseBtn.SetActive(false);
        if (PhotonNetwork.PlayerList.Length > 1 && !seeker_available && (!red_available || !green_available || !blue_available))
        {
            StartButton.SetActive(PhotonNetwork.IsMasterClient);
        }
    }
    [PunRPC]
    public void BlockRed(string name)
    {
        red_available = false;
        RedNick.text = name;
        RedChooseBtn.SetActive(false);
        if (PhotonNetwork.PlayerList.Length > 1 && !seeker_available && (!red_available || !green_available || !blue_available))
        {
            StartButton.SetActive(PhotonNetwork.IsMasterClient);
        }
    }
    [PunRPC]
    public void BlockGreen(string name)
    {
        green_available = false;
        GreenNick.text = name;
        GreenChooseBtn.SetActive(false);
        if (PhotonNetwork.PlayerList.Length > 1 && !seeker_available && (!red_available || !green_available || !blue_available))
        {
            StartButton.SetActive(PhotonNetwork.IsMasterClient);
        }
    }
    [PunRPC]
    public void BlockBlue(string name)
    {
        blue_available = false;
        BlueNick.text = name;
        BlueChooseBtn.SetActive(false);
        if (PhotonNetwork.PlayerList.Length > 1 && !seeker_available && (!red_available || !green_available || !blue_available))
        {
            StartButton.SetActive(PhotonNetwork.IsMasterClient);
        }
    }
    [PunRPC]
    public void UnblockSeeker()
    {
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        seeker_available = true;
        SeekerNick.text = "";
        if (player_data.GetPlayerRole() == PlayerData.PlayerRole.None) SeekerChooseBtn.SetActive(true);
        if (PhotonNetwork.PlayerList.Length == 1 || seeker_available || (red_available && green_available && blue_available))
        {
            StartButton.SetActive(false);
        }
    }
    [PunRPC]
    public void UnblockRed()
    {
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        red_available = true;
        RedNick.text = "";
        if (player_data.GetPlayerRole() == PlayerData.PlayerRole.None) RedChooseBtn.SetActive(true);
        if (PhotonNetwork.PlayerList.Length == 1 || seeker_available || (red_available && green_available && blue_available))
        {
            StartButton.SetActive(false);
        }
    }
    [PunRPC]
    public void UnblockGreen()
    {
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        green_available = true;
        GreenNick.text = "";
        if (player_data.GetPlayerRole() == PlayerData.PlayerRole.None) GreenChooseBtn.SetActive(true);
        if (PhotonNetwork.PlayerList.Length == 1 || seeker_available || (red_available && green_available && blue_available))
        {
            StartButton.SetActive(false);
        }
    }
    [PunRPC]
    public void UnblockBlue()
    {
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        blue_available = true;
        BlueNick.text = "";
        if (player_data.GetPlayerRole() == PlayerData.PlayerRole.None) BlueChooseBtn.SetActive(true);
        if (PhotonNetwork.PlayerList.Length == 1 || seeker_available || (red_available && green_available && blue_available))
        {
            StartButton.SetActive(false);
        }
    }

    void Start()
    {
        Debug.Log("Scene loaded: Lobby");
        RoomNameText.text = PhotonNetwork.CurrentRoom.Name;
        int playerCount = PhotonNetwork.PlayerList.Length;
        CurPlText.text = playerCount.ToString();
        if (PhotonNetwork.IsMasterClient)
        {
            GenerateRoomsPanel.SetActive(true);
        } else
        {
            GenerateRoomsPanel.SetActive(false);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (MenuUi.activeInHierarchy)
            {
                MenuUi.SetActive(false);
                LobbyInfo.SetActive(true);
                NickNamePanel.SetActive(true);
            }
            else
            {
                MenuUi.SetActive(true);
                LobbyInfo.SetActive(false);
                NickNamePanel.SetActive(false);
            }
        }
    }

    public override void OnJoinedRoom()
    {
        
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        int playerCount = PhotonNetwork.PlayerList.Length;
        CurPlText.text = playerCount.ToString();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        int playerCount = PhotonNetwork.PlayerList.Length;
        CurPlText.text = playerCount.ToString();
        if (PhotonNetwork.PlayerList.Length > 1 && !seeker_available && (!red_available || !green_available || !blue_available))
        {
            StartButton.SetActive(PhotonNetwork.IsMasterClient);
        }
    }

    public void StartGame()
    {
        PhotonNetwork.LoadLevel("Game");
    }

    public void ChooseSeeker()
    {
        if (!seeker_available) return;
        player_model = PhotonNetwork.Instantiate("SeekerLobbyPrefabV1", seeker_spawn_position.transform.position, Quaternion.AngleAxis(180, Vector3.down));
        photonView.RPC("BlockSeeker", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        player_data.SetPlayerRole(PlayerData.PlayerRole.Seeker);
        OffAllChoosing();
        SeekerLeaveBtn.SetActive(true);
    }
    public void ChooseRed()
    {
        if (!red_available) return;
        player_model = PhotonNetwork.Instantiate("RedHiderLobbyPrefabV1", hiders_spawn_positions[0].transform.position, Quaternion.AngleAxis(180, Vector3.down));
        photonView.RPC("BlockRed", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        player_data.SetPlayerRole(PlayerData.PlayerRole.Red);
        OffAllChoosing();
        RedLeaveBtn.SetActive(true);

    }
    public void ChooseGreen()
    {
        if (!green_available) return;
        player_model = PhotonNetwork.Instantiate("GreenHiderLobbyPrefabV1", hiders_spawn_positions[1].transform.position, Quaternion.AngleAxis(180, Vector3.down));
        photonView.RPC("BlockGreen", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        player_data.SetPlayerRole(PlayerData.PlayerRole.Green);
        OffAllChoosing();
        GreenLeaveBtn.SetActive(true);
    }
    public void ChooseBlue()
    {
        if (!blue_available) return;
        player_model = PhotonNetwork.Instantiate("BlueHiderLobbyPrefabV1", hiders_spawn_positions[2].transform.position, Quaternion.AngleAxis(180, Vector3.down));
        photonView.RPC("BlockBlue", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        player_data.SetPlayerRole(PlayerData.PlayerRole.Blue);
        OffAllChoosing();
        BlueLeaveBtn.SetActive(true);
    }

    private void OnChoosing()
    {
        SeekerChooseBtn.SetActive(seeker_available);
        RedChooseBtn.SetActive(red_available);
        GreenChooseBtn.SetActive(green_available);
        BlueChooseBtn.SetActive(blue_available);
    }

    private void OffAllChoosing()
    {
        SeekerChooseBtn.SetActive(false);
        RedChooseBtn.SetActive(false);
        GreenChooseBtn.SetActive(false);
        BlueChooseBtn.SetActive(false);
    }
    public void LeaveSeeker()
    {
        SeekerLeaveBtn.SetActive(false);
        player_data.SetPlayerRole(PlayerData.PlayerRole.None);
        PhotonNetwork.Destroy(player_model);
        photonView.RPC("UnblockSeeker", RpcTarget.AllBuffered);
        OnChoosing();

    }
    public void LeaveRed()
    {
        RedLeaveBtn.SetActive(false);
        player_data.SetPlayerRole(PlayerData.PlayerRole.None);
        PhotonNetwork.Destroy(player_model);
        photonView.RPC("UnblockRed", RpcTarget.AllBuffered);
        OnChoosing();
    }
    public void LeaveGreen()
    {
        GreenLeaveBtn.SetActive(false);
        player_data.SetPlayerRole(PlayerData.PlayerRole.None);
        PhotonNetwork.Destroy(player_model);
        photonView.RPC("UnblockGreen", RpcTarget.AllBuffered);
        OnChoosing();
    }
    public void LeaveBlue()
    {
        BlueLeaveBtn.SetActive(false);
        player_data.SetPlayerRole(PlayerData.PlayerRole.None);
        PhotonNetwork.Destroy(player_model);
        photonView.RPC("UnblockBlue", RpcTarget.AllBuffered);
        OnChoosing();
    }

    void UnblockCharacter(PlayerData.PlayerRole role)
    {
        if (role == PlayerData.PlayerRole.None) return;

        if (role == PlayerData.PlayerRole.Seeker)
        {
            photonView.RPC("UnblockSeeker", RpcTarget.AllBuffered);
        }
        else if (role == PlayerData.PlayerRole.Red)
        {
            photonView.RPC("UnblockRed", RpcTarget.AllBuffered);
        }
        else if (role == PlayerData.PlayerRole.Green)
        {
            photonView.RPC("UnblockGreen", RpcTarget.AllBuffered);
        }
        else if (role == PlayerData.PlayerRole.Blue)
        {
            photonView.RPC("UnblockBlue", RpcTarget.AllBuffered);
        }

    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.InRoom)
        {
            player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
            UnblockCharacter(player_data.GetPlayerRole());
            PhotonNetwork.LeaveRoom();
        }
    }

    public override void OnLeftRoom()
    {
        Debug.Log("Leaving room");
        PhotonNetwork.LoadLevel("MainMenu");
    }

    public void UpdateGenerateRoom()
    {
        player_data = GameObject.Find("LobbyController").GetComponent<PlayerData>();
        player_data.SetIsRoom(GenerateRoomToggle.isOn);
    }

}
