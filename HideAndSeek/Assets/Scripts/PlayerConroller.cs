using Photon.Pun;
using System;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using System.Collections.Generic;

[RequireComponent(typeof(MazeConstructor))]

public class PlayerController : MonoBehaviourPunCallbacks
{

    public float speed;
    public float run_speed;
    public float jump_force;
    public float sensivity;
    public Camera player_camera;
    public bool on_ground;
    public PlayerController player_script;
    public GameObject tab_panel;
    public GameObject start_timer_panel;
    public TMP_Text start_seconds_left;
    public GameObject game_over_panel;
    public GameObject results_panel;
    public GameObject game_timer_panel;
    public TMP_Text[] result_nicknames;
    public TMP_Text[] result_points;
    public PlayerData.PlayerRole role;
    public float[] afk_place;
    public TMP_Text time_left;

    private Animator player_anamator;
    private Rigidbody player_rigidbody;
    private PhotonView player_view;
    private MazeConstructor generator;
    private float mouse_x;
    private float mouse_y;
    private float start_time_remaining;
    private int points = 0;
    private int alive_players;
    private float round_time = 0;
    private int total_players;
    private bool game_ended = false;
    private bool im_alive = true;
    private bool is_room = true;

    private float kDiagonalSpeed = 0.7071f;

    [PunRPC]
    public void GenerateMaze(string s, Vector3 start_position, bool iz_room)
    {
        generator = GameObject.Find("GameManager").GetComponent<MazeConstructor>();
        try
        {
            var seeker_transform = GameObject.Find("SeekerPrefabV1(Clone)").GetComponent<Transform>();
            if (iz_room)
            {
                seeker_transform.position = new Vector3(0.25f, 0, 0.25f);
            } else
            {
                seeker_transform.position = start_position + new Vector3(-1, 0, -1);
            }
        }
        catch
        {
            Debug.LogWarning("No_Seeker");
        }
        try
        {
            var red_transform = GameObject.Find("RedHiderPrefabV1(Clone)").GetComponent<Transform>();
            if (iz_room)
            {
                red_transform.position = new Vector3(2.25f, 0, 0.25f);
            } else
            {
                red_transform.position = start_position + new Vector3(1, 0, -1);
            }
        }
        catch
        {
            Debug.LogWarning("No_Red");
        }
        try
        {
            var blue_transform = GameObject.Find("BlueHiderPrefabV1(Clone)").GetComponent<Transform>();
            if (iz_room)
            {
                blue_transform.position = new Vector3(0.25f, 0, 2.25f);
            }
            else
            {
                blue_transform.position = start_position + new Vector3(-1, 0, 1);
            }
        }
        catch
        {
            Debug.LogWarning("No_Blue");
        }
        try
        {
            var green_transform = GameObject.Find("GreenHiderPrefabV1(Clone)").GetComponent<Transform>();
            if (iz_room)
            {
                green_transform.position = new Vector3(2.25f, 0, 2.25f);
            }
            else
            {
                green_transform.position = start_position + new Vector3(1, 0, 1);
            }
        }
        catch
        {
            Debug.LogWarning("No_Green");
        }
        generator.DisplayMaze(s, iz_room);
    }

    private void Awake()
    {
        player_view = GetComponent<PhotonView>();
        if (!player_view.IsMine)
        {
            player_camera.gameObject.SetActive(false);
            player_script.enabled = false;
        }
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        player_rigidbody = GetComponent<Rigidbody>();
        player_anamator = GetComponent<Animator>();

        if (role == PlayerData.PlayerRole.Seeker)
        {
            start_time_remaining = 15f;
        }
        else
        {
            start_time_remaining = 5f;
        }
        float seconds = Mathf.FloorToInt(start_time_remaining % 60);
        start_seconds_left.text = (seconds + 1).ToString();

        alive_players = PhotonNetwork.CurrentRoom.PlayerCount - 1;
        total_players = PhotonNetwork.CurrentRoom.PlayerCount;

        PlayerData player_data = GameObject.Find("GameManager").GetComponent<PlayerData>();
        round_time = player_data.GetRoundTime() + 15f;
        is_room = player_data.GetIsRoom();
        Debug.LogWarning(is_room);

        if (PhotonNetwork.IsMasterClient)
        {
            generator = GameObject.Find("GameManager").GetComponent<MazeConstructor>();
            string s = generator.GenerateNewMaze(is_room);
            Vector3 start_position = generator.GetStartPosition();
            photonView.RPC("GenerateMaze", RpcTarget.AllBuffered, s, start_position, is_room);
        }
    }

    void FixedUpdate()
    {
        if (game_ended) return;
        if (!im_alive) return;

        if (start_time_remaining > 0)
        {
            return;
        }
        MakeMove();
    }

    private void Update()
    {
        if (alive_players == 0)
        {
            game_ended = true;
            results_panel.SetActive(true);
            return;
        }
        if (round_time > 0)
        {
            round_time -= Time.deltaTime;
            float minutes_left = Mathf.FloorToInt((round_time + Time.deltaTime) / 60 % 60);
            float seconds_left = Mathf.FloorToInt((round_time + Time.deltaTime) % 60);
            string minutes = minutes_left.ToString();
            string seconds = seconds_left.ToString();
            if (minutes_left < 10)
            {
                minutes = '0' + minutes;
            }
            if (seconds_left < 10)
            {
                seconds = '0' + seconds;
            }
            time_left.text = minutes + ':' + seconds;
        }
        if (round_time <= 0)
        {
            game_ended = true;
            results_panel.SetActive(true);
        }
        if (start_time_remaining > 0)
        {
            start_time_remaining -= Time.deltaTime;
            float seconds = Mathf.FloorToInt((start_time_remaining + Time.deltaTime) % 60);
            start_seconds_left.text = seconds.ToString();
            return;
        }
        if (start_timer_panel.activeInHierarchy)
        {
            start_timer_panel.SetActive(false);
        }
        //CheckTab();
    }

    private void MakeMove()
    {
        bool is_idle = true;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            is_idle = true;
                        }
                        else
                        {
                            transform.localPosition -= transform.right * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.forward * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            transform.localPosition -= transform.right * run_speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition += transform.forward * run_speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                }
                else
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            is_idle = true;
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * run_speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition += transform.forward * run_speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            transform.localPosition += transform.forward * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                }
            } else
            {
                if (Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition -= transform.forward * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            transform.localPosition -= transform.right * run_speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition -= transform.forward * run_speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            is_idle = true;
                        }
                        else
                        {
                            transform.localPosition -= transform.right * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                }
                else
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * run_speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition -= transform.forward * run_speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            transform.localPosition -= transform.forward * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * run_speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(2);
                        }
                        else
                        {
                            is_idle = true;
                        }
                    }
                }
            }
        } else
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            is_idle = true;
                        }
                        else
                        {
                            transform.localPosition -= transform.right * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.forward * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            transform.localPosition -= transform.right * speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition += transform.forward * speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                }
                else
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            is_idle = true;
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition += transform.forward * speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            transform.localPosition += transform.forward * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition -= transform.forward * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            transform.localPosition -= transform.right * speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition -= transform.forward * speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            is_idle = true;
                        }
                        else
                        {
                            transform.localPosition -= transform.right * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                }
                else
                {
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * speed * kDiagonalSpeed * Time.deltaTime;
                            transform.localPosition -= transform.forward * speed * kDiagonalSpeed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            transform.localPosition -= transform.forward * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.D))
                        {
                            transform.localPosition += transform.right * speed * Time.deltaTime;
                            is_idle = false;
                            SetAnimationIdx(1);
                        }
                        else
                        {
                            is_idle = true;
                        }
                    }
                }
            }
        }

        if (is_idle)
        {
            SetAnimationIdx(0);
        }

        mouse_x = Input.GetAxis("Mouse X") * sensivity * Time.deltaTime;
        mouse_y += Input.GetAxis("Mouse Y") * sensivity * Time.deltaTime;
        mouse_y = Mathf.Clamp(mouse_y, -90, 90);
        transform.Rotate(mouse_x * new Vector3(0, 1, 0));
        player_camera.transform.localEulerAngles = new Vector3(-mouse_y, 0, 0);

        if (Input.GetKey(KeyCode.Space))
        {
            if (on_ground)
            {
                player_rigidbody.AddForce(transform.up * jump_force);
            }
        }
    }

    private void CheckTab()
    {
        if (Input.GetKey(KeyCode.Tab))
        {
            tab_panel.SetActive(true);
        } else
        {
            tab_panel.SetActive(false);
        }
    }

    [PunRPC]
    public void HiderCought(string hider_tag, string hider_name)
    {
        if (hider_tag == "Red")
        {
            result_nicknames[1].text = hider_tag;
            result_points[1].text = (total_players - alive_players).ToString();
            --alive_players;
        }
        if (hider_tag == "Green")
        {
            result_nicknames[2].text = hider_tag;
            result_points[2].text = (total_players - alive_players).ToString();
            --alive_players;
        }
        if (hider_tag == "Blue")
        {
            result_nicknames[3].text = hider_tag;
            result_points[3].text = (total_players - alive_players).ToString();
            --alive_players;
        }
        if (alive_players == 0)
        {
            result_nicknames[0].text = hider_tag;
            result_points[0].text = total_players.ToString();
            game_ended = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            on_ground = false;  
        }
        if (collision.gameObject.tag == "Seeker")
        {
            if (gameObject.tag == "Red")
            {
                player_view.RPC("HiderCought", RpcTarget.AllBuffered, "Red", PhotonNetwork.LocalPlayer.NickName);
            }
            if (gameObject.tag == "Green")
            {
                player_view.RPC("HiderCought", RpcTarget.AllBuffered, "Green", PhotonNetwork.LocalPlayer.NickName);
            }
            if (gameObject.tag == "Blue")
            {
                player_view.RPC("HiderCought", RpcTarget.AllBuffered, "Blue", PhotonNetwork.LocalPlayer.NickName);
            }
            game_over_panel.SetActive(true);
            im_alive = false;
            transform.position = new Vector3(afk_place[0], afk_place[1], afk_place[2]);
        }
        if (gameObject.tag == "Seeker")
        {
            if (collision.gameObject.tag == "Red")
            {
                result_nicknames[1].text = "Red";
                result_points[1].text = (total_players - alive_players).ToString();
                --alive_players;
            }
            if (collision.gameObject.tag == "Green")
            {
                result_nicknames[2].text = "Green";
                result_points[2].text = (total_players - alive_players).ToString();
                --alive_players;
            }
            if (collision.gameObject.tag == "Blue")
            {
                result_nicknames[3].text = "Blue";
                result_points[3].text = (total_players - alive_players).ToString();
                --alive_players;
            }
        }
    }

    void SetAnimationIdx(int idx)
    {
        player_anamator.SetInteger("Animation_Idx", idx);
    }
}
