using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;
using WebSocketSharp;
using System.Security.Cryptography;
using UnityEngine.UIElements;

[RequireComponent(typeof(PlayerData))]

public class PlayerManager : MonoBehaviourPunCallbacks
{

    private PlayerData player_data;

    public static PlayerManager instance;
    void Start()
    {
        if (instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.name == "Game")
        {
            player_data = GameObject.Find("GameManager").GetComponent<PlayerData>();
            if (player_data.GetPlayerRole() == PlayerData.PlayerRole.Seeker)
            {
                PhotonNetwork.Instantiate("SeekerPrefabV1", new Vector3(50, 50, 50), Quaternion.identity);
                Debug.Log("Seeker spawned");
            }
            else if(player_data.GetPlayerRole() == PlayerData.PlayerRole.Red)
            {
                PhotonNetwork.Instantiate("RedHiderPrefabV1", new Vector3(20, 20, 20), Quaternion.identity);
                Debug.Log("Red spawned");
            }
            else if (player_data.GetPlayerRole() == PlayerData.PlayerRole.Green)
            {
                PhotonNetwork.Instantiate("RedHiderPrefabV1", new Vector3(10, 10, 10), Quaternion.identity);
                Debug.Log("Green spawned");
            }
            else if (player_data.GetPlayerRole() == PlayerData.PlayerRole.Blue)
            {
                PhotonNetwork.Instantiate("BlueHiderPrefabV1", Vector3.zero, Quaternion.identity);
                Debug.Log("Blue spawned");
            } else
            {
                Debug.LogError("player_role set to None, can`t instantiate the character");
                return;
            }
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();
    }

}
