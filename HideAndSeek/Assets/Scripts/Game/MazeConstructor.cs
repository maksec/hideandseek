using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeConstructor : MonoBehaviour
{
    public bool showDebug;
    public bool generateRoom;
    public float placementThreshold;
    public int minWidthRoom;
    public int widthPassage;
    public int minCrossingWidth;
    public float widthMaze;
    public float heightMaze;
    public int sizeRows;
    public int sizeCol;

    private MazeDataGenerator dataGenerator;
    private MazeMeshGenerator meshGenerator;

    [SerializeField] private Material mazeMat1;
    [SerializeField] private Material mazeMat2;

    public int[,] data
    {
        get; private set;
    }

    public int startRow
    {
        get; private set;
    }
    public int startCol
    {
        get; private set;
    }
    public float hallWidth
    {
        get; private set;
    }
    public float hallHeight
    {
        get; private set;
    }

    void Awake()
    {
        dataGenerator = new MazeDataGenerator(placementThreshold, minWidthRoom, widthPassage, minCrossingWidth);
        meshGenerator = new MazeMeshGenerator();
        meshGenerator.height = heightMaze;
        meshGenerator.width = widthMaze;
        data = new int[,]
        {
            {1, 1, 1},
            {1, 0, 1},
            {1, 1, 1}
        };
        sizeCol = 13;
        sizeRows = 15;
    }

    private void FindStartPosition()
    {
        int[,] maze = data;
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (maze[i, j] == 0)
                {
                    startRow = i;
                    startCol = j;
                    return;
                }
            }
        }
    }

    public Vector3 GetStartPosition()
    {
        return new Vector3(startCol * hallWidth, .5f, startRow * hallWidth);
    }

    public void DisplayMaze(string s, bool is_room)
    {
        GameObject go = new GameObject();
        go.transform.position = Vector3.zero;
        go.name = "Procedural Maze";
        go.tag = "Generated";

        if (is_room)
        {
            meshGenerator.width = 1;
            sizeCol = 63;
            sizeRows = 65;
            generateRoom = true;
        }

        int[,] maze = new int[sizeRows, sizeCol];
        for (int i=0; i<s.Length; i++)
        {
            maze[i / sizeCol, i % sizeCol] = s[i] == '0' ? 0 : 1;
        }
        MeshFilter mf = go.AddComponent<MeshFilter>();
        mf.mesh = meshGenerator.FromData(maze);

        MeshCollider mc = go.AddComponent<MeshCollider>();
        mc.sharedMesh = mf.mesh;

        MeshRenderer mr = go.AddComponent<MeshRenderer>();
        mr.materials = new Material[2] { mazeMat1, mazeMat2 };
    }

    public string GenerateNewMaze(bool is_room)
    {
        if (is_room)
        {
            meshGenerator.width = 1;
            sizeCol = 63;
            sizeRows = 65;
            generateRoom = true;
        }
        if (sizeRows % 2 == 0 && sizeCol % 2 == 0)
        {
            Debug.LogError("Odd numbers work better for dungeon size.");
        }

        data = generateRoom ? dataGenerator.FromDimensionsRoom(sizeRows, sizeCol) : dataGenerator.FromDimensions(sizeRows, sizeCol);

        hallWidth = meshGenerator.width;
        hallHeight = meshGenerator.height;

        FindStartPosition();
        string s = "";
        for (int i = 0; i <= data.GetUpperBound(0); i++)
        {
            for (int j = 0; j <= data.GetUpperBound(1); j++)
            {
                s += data[i, j];
            }
        }
        return s;
    }

    void OnGUI()
    {
        if (!showDebug)
        {
            return;
        }

        int[,] maze = data;
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        string msg = "";

        for (int i = rMax; i >= 0; i--)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (maze[i, j] == 0)
                {
                    msg += "0";
                }
                else
                {
                    msg += "1";
                }
            }
            msg += "\n";
        }

        GUI.Label(new Rect(20, 20, 500, 500), msg);
    }
}
