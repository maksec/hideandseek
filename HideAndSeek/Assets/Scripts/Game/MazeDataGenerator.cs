using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeDataGenerator
{
    public float placementThreshold;

    public int minWidthRoom;

    public int widthPassage;

    public int minCrossingWidth;

    struct Room
    {
        public int first_shift;
        public int second_shift;
        public int length;
        public int width;

        public Room(int first_shift, int second_shift, int length, int width)
        {
            this.first_shift = first_shift;
            this.second_shift = second_shift;
            this.length = length;
            this.width = width;
        }
    }

    private List<Room> rooms;

    public MazeDataGenerator()
    {
        placementThreshold = .01f;
        minWidthRoom = 11;
        widthPassage = 3;
        minCrossingWidth = 5;
        rooms = new List<Room>();
    }

    public MazeDataGenerator(float placement_threshold, int min_width_room, int width_passage, int min_crossing_width)
    {
        placementThreshold = placement_threshold;
        minWidthRoom = min_width_room;
        widthPassage = width_passage;
        minCrossingWidth = min_crossing_width;
        rooms = new List<Room>();
    }

    public int[,] FromDimensions(int sizeRows, int sizeCols)
    {
        int[,] maze = new int[sizeRows, sizeCols];
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {
                if (i == 0 || j == 0 || i == rMax || j == cMax)
                {
                    maze[i, j] = 1;
                }

                else if (i % 2 == 0 && j % 2 == 0)
                {
                    if (Random.value > placementThreshold)
                    {
                        maze[i, j] = 1;

                        int a = Random.value < .5 ? 0 : (Random.value < .5 ? -1 : 1);
                        int b = a != 0 ? 0 : (Random.value < .5 ? -1 : 1);
                        maze[i + a, j + b] = 1;
                    }
                }
            }
        }
        return maze;
    }

    private List<(int, int)> DivisionIntoRoom(int[,] maze, int first_shift, int second_shift)
    {
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);
        List<(int, int)> walls = new List<(int, int)>();
        if (rMax < 2 * minWidthRoom && cMax < 2 * minWidthRoom)
        {
            rooms.Add(new Room(first_shift, second_shift, rMax, cMax));
            return walls;
        }

        if(rMax < 2 * minWidthRoom)
        {
            int wall = (int)(Random.value * (double)(cMax - 2 * minWidthRoom)) + minWidthRoom;
            int[,] firstMaze = new int[rMax + 1, wall];
            int[,] secondMaze = new int[rMax + 1, cMax - wall];

            for (int i = 0; i <= rMax; i++)
            {
                walls.Add((i + first_shift, wall + second_shift));
            }

            List<(int, int)> newWalls1 = DivisionIntoRoom(firstMaze, first_shift, second_shift);
            walls.AddRange(newWalls1);
            List<(int, int)> newWalls2 = DivisionIntoRoom(secondMaze, first_shift, second_shift + wall + 1);
            walls.AddRange(newWalls2);
            return walls;
        }

        if (cMax < 2 * minWidthRoom)
        {
            int wall = (int)(Random.value * (double)(rMax - 2 * minWidthRoom)) + minWidthRoom;
            int[,] firstMaze = new int[wall, cMax + 1];
            int[,] secondMaze = new int[rMax - wall, cMax + 1];

            for (int i = 0; i <= cMax; i++)
            {
                walls.Add((wall + first_shift, i + second_shift));
            }

            List<(int, int)> newWalls1 = DivisionIntoRoom(firstMaze, first_shift, second_shift);
            walls.AddRange(newWalls1);
            List<(int, int)> newWalls2 = DivisionIntoRoom(secondMaze, first_shift + wall + 1, second_shift);
            walls.AddRange(newWalls2);
            return walls;
        }

            if (Random.value < .5)
        {
            int wall = (int)(Random.value * (double)(rMax - 2 * minWidthRoom)) + minWidthRoom;
            int[,] firstMaze = new int[wall, cMax + 1];
            int[,] secondMaze = new int[rMax - wall, cMax + 1];

            for(int i = 0; i <= cMax; i++)
            {
                walls.Add((wall + first_shift, i + second_shift));
            }

            List<(int, int)> newWalls1 = DivisionIntoRoom(firstMaze, first_shift, second_shift);
            walls.AddRange(newWalls1);
            List<(int, int)> newWalls2 = DivisionIntoRoom(secondMaze, first_shift + wall + 1, second_shift);
            walls.AddRange(newWalls2);
        }
        else
        {
            int wall = (int)(Random.value * (double)(cMax - 2 * minWidthRoom)) + minWidthRoom;
            int[,] firstMaze = new int[rMax + 1, wall];
            int[,] secondMaze = new int[rMax + 1, cMax - wall];

            for (int i = 0; i <= rMax; i++)
            {
                walls.Add((i + first_shift, wall + second_shift));
            }

            List<(int, int)> newWalls1 = DivisionIntoRoom(firstMaze, first_shift, second_shift);
            walls.AddRange(newWalls1);
            List<(int, int)> newWalls2 = DivisionIntoRoom(secondMaze, first_shift, second_shift + wall + 1);
            walls.AddRange(newWalls2);
        }
        return walls;
    }

    public int[,] FromDimensionsRoom(int sizeRows, int sizeCols)
    {
        int[,] maze = new int[sizeRows, sizeCols];
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);
        List<(int, int)> wall = DivisionIntoRoom(maze, 0, 0);
        for(int i = 0; i < wall.Count; i++)
        {
            maze[wall[i].Item1, wall[i].Item2] = 1;
        }
        List<(int, int)> related_rooms_id = new List<(int, int)>();
        for (int i = 0; i < rooms.Count; i++)
        {
            Dictionary<int, int> count_common_wall = new Dictionary<int, int>();
            if (rooms[i].first_shift > 1)
            {
                for(int j = 0; j < rooms[i].width; j++)
                {
                    if (maze[rooms[i].first_shift - 2, rooms[i].second_shift + j] == 0)
                    {
                        for(int k = 0; k < rooms.Count; k++)
                        {
                            if((rooms[i].first_shift - 2 == rooms[k].first_shift + rooms[k].length)
                                && ((rooms[i].second_shift + j <= rooms[k].second_shift + rooms[k].width)
                                && (rooms[i].second_shift + j >= rooms[k].second_shift)))
                            {
                                if (count_common_wall.ContainsKey(k))
                                {
                                    count_common_wall[k] += 1;
                                }
                                else
                                {
                                    count_common_wall.Add(k, 1);
                                }
                            }
                        }
                    }
                }
            }

            if (rooms[i].second_shift > 1)
            {
                for (int j = 0; j < rooms[i].length; j++)
                {
                    if (maze[rooms[i].first_shift + j, rooms[i].second_shift - 2] == 0)
                    {
                        for (int k = 0; k < rooms.Count; k++)
                        {
                            if ((rooms[i].second_shift - 2 == rooms[k].second_shift + rooms[k].width)
                                && ((rooms[i].first_shift + j <= rooms[k].first_shift + rooms[k].length)
                                && (rooms[i].first_shift + j >= rooms[k].first_shift)))
                            {
                                if (count_common_wall.ContainsKey(k))
                                {
                                    count_common_wall[k] += 1;
                                }
                                else
                                {
                                    count_common_wall.Add(k, 1);
                                }
                            }
                        }
                    }
                }
            }

            if (rooms[i].second_shift + rooms[i].width < cMax - 1)
            {
                for (int j = 0; j < rooms[i].length; j++)
                {
                    if (maze[rooms[i].first_shift + j, rooms[i].second_shift + rooms[i].width + 2] == 0)
                    {
                        for (int k = 0; k < rooms.Count; k++)
                        {
                            if ((rooms[i].second_shift + rooms[i].width + 2 == rooms[k].second_shift)
                                && ((rooms[i].first_shift + j <= rooms[k].first_shift + rooms[k].length)
                                && (rooms[i].first_shift + j >= rooms[k].first_shift)))
                            {
                                if (count_common_wall.ContainsKey(k))
                                {
                                    count_common_wall[k] += 1;
                                }
                                else
                                {
                                    count_common_wall.Add(k, 1);
                                }
                            }
                        }
                    }
                }
            }

            if (rooms[i].first_shift + rooms[i].length < rMax - 1)
            {
                for (int j = 0; j < rooms[i].width; j++)
                {
                    if (maze[rooms[i].first_shift + rooms[i].length + 2, rooms[i].second_shift + j] == 0)
                    {
                        for (int k = 0; k < rooms.Count; k++)
                        {
                            if ((rooms[i].first_shift + rooms[i].length + 2 == rooms[k].first_shift)
                                && ((rooms[i].second_shift + j <= rooms[k].second_shift + rooms[k].width)
                                && (rooms[i].second_shift + j >= rooms[k].second_shift)))
                            {
                                if (count_common_wall.ContainsKey(k))
                                {
                                    count_common_wall[k] += 1;
                                }
                                else
                                {
                                    count_common_wall.Add(k, 1);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var common_wall in count_common_wall)
            {
                if (common_wall.Value >= minCrossingWidth)
                {
                    if (!related_rooms_id.Contains((common_wall.Key, i)))
                    {
                        related_rooms_id.Add((i, common_wall.Key));
                    }
                }
            }
        }

        List<int> related_vertex = new List<int>();
        List<int> not_related_vertex = new List<int>();
        List<(int, int)> final_related_rooms = new List<(int, int)>();
        int head_vertex = (int)Mathf.Round(Random.value * (rooms.Count - 1));
        related_vertex.Add(head_vertex);
        for(int i = 0; i < rooms.Count; i++)
        {
            if(i != head_vertex)
            {
                not_related_vertex.Add(i);
            }
        }
        while(not_related_vertex.Count > 0)
        {
            List<int> not_all_related_vertex = new List<int>();
            for(int i = 0; i < related_rooms_id.Count; i++)
            {
                if (related_vertex.Contains(related_rooms_id[i].Item1) && not_related_vertex.Contains(related_rooms_id[i].Item2) &&
                    !not_all_related_vertex.Contains(related_rooms_id[i].Item1))
                {
                    not_all_related_vertex.Add(related_rooms_id[i].Item1);
                }

                if (related_vertex.Contains(related_rooms_id[i].Item2) && not_related_vertex.Contains(related_rooms_id[i].Item1) &&
                    !not_all_related_vertex.Contains(related_rooms_id[i].Item2))
                {
                    not_all_related_vertex.Add(related_rooms_id[i].Item2);
                }
            }
            int selected_vertex = not_all_related_vertex[(int)Mathf.Round(Random.value * (not_all_related_vertex.Count - 1))];
            List<int> not_related_neighboring_vertex = new List<int>();
            for(int i = 0; i < related_rooms_id.Count; i++)
            {
                if(related_rooms_id[i].Item1 == selected_vertex && not_related_vertex.Contains(related_rooms_id[i].Item2) 
                    && !not_related_neighboring_vertex.Contains(related_rooms_id[i].Item2))
                {
                    not_related_neighboring_vertex.Add(related_rooms_id[i].Item2);
                }

                if (related_rooms_id[i].Item2 == selected_vertex && not_related_vertex.Contains(related_rooms_id[i].Item1)
                    && !not_related_neighboring_vertex.Contains(related_rooms_id[i].Item1))
                {
                    not_related_neighboring_vertex.Add(related_rooms_id[i].Item1);
                }
            }
            int selected_not_related_vertex = not_related_neighboring_vertex[(int)Mathf.Round(Random.value * (not_related_neighboring_vertex.Count - 1))];
            final_related_rooms.Add((selected_vertex, selected_not_related_vertex));
            related_vertex.Add(selected_not_related_vertex);
            not_related_vertex.Remove(selected_not_related_vertex);
        }

        related_rooms_id = final_related_rooms;
        
        string s3 = "";
        for (int i=0; i < final_related_rooms.Count; i++)
        {
            s3 += "( " + final_related_rooms[i].Item1 + " , " + final_related_rooms[i].Item2 + " )";
        }
        Debug.Log(s3);
        for (int i = 0; i < related_rooms_id.Count; i++)
        {
            if ((rooms[related_rooms_id[i].Item1].second_shift >= rooms[related_rooms_id[i].Item2].second_shift) &&
                (rooms[related_rooms_id[i].Item1].second_shift <= rooms[related_rooms_id[i].Item2].second_shift +
                rooms[related_rooms_id[i].Item2].width))
            {
                if (rooms[related_rooms_id[i].Item1].first_shift < rooms[related_rooms_id[i].Item2].first_shift)
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift
                    + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item2].second_shift +
                    rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                    + rooms[related_rooms_id[i].Item1].second_shift] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift
                        + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item2].second_shift +
                        rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                        + rooms[related_rooms_id[i].Item1].second_shift + j / 2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift
                        + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item2].second_shift +
                        rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                        + rooms[related_rooms_id[i].Item1].second_shift - j / 2 - 1] = 0;
                    }
                }
                else
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item2].second_shift +
                    rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                    + rooms[related_rooms_id[i].Item1].second_shift] = 0;
                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item2].second_shift +
                        rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                        + rooms[related_rooms_id[i].Item1].second_shift + j / 2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item2].second_shift +
                        rooms[related_rooms_id[i].Item2].width - rooms[related_rooms_id[i].Item1].second_shift) / 2
                        + rooms[related_rooms_id[i].Item1].second_shift - j / 2 - 1] = 0;
                    }
                }
                continue;
            }

            if (rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width >= rooms[related_rooms_id[i].Item2].second_shift &&
                rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width <= rooms[related_rooms_id[i].Item2].second_shift +
                rooms[related_rooms_id[i].Item2].width)
            {
                if (rooms[related_rooms_id[i].Item1].first_shift < rooms[related_rooms_id[i].Item2].first_shift)
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift
                    + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item1].second_shift +
                    rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                    + rooms[related_rooms_id[i].Item2].second_shift] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift
                        + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item1].second_shift +
                        rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                        + rooms[related_rooms_id[i].Item2].second_shift + j / 2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift
                        + rooms[related_rooms_id[i].Item1].length + 1, (rooms[related_rooms_id[i].Item1].second_shift +
                        rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                        + rooms[related_rooms_id[i].Item2].second_shift - j / 2 - 1] = 0;
                    }
                }
                else
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item1].second_shift +
                    rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                    + rooms[related_rooms_id[i].Item2].second_shift] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item1].second_shift +
                        rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                        + rooms[related_rooms_id[i].Item2].second_shift + j / 2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1, (rooms[related_rooms_id[i].Item1].second_shift +
                        rooms[related_rooms_id[i].Item1].width - rooms[related_rooms_id[i].Item2].second_shift) / 2
                        + rooms[related_rooms_id[i].Item2].second_shift - j / 2 - 1] = 0;
                    }
                }
                continue;
            }

            if(rooms[related_rooms_id[i].Item1].second_shift < rooms[related_rooms_id[i].Item2].second_shift &&
               rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width >
               rooms[related_rooms_id[i].Item2].second_shift + rooms[related_rooms_id[i].Item2].width)
            {
                if (rooms[related_rooms_id[i].Item1].first_shift < rooms[related_rooms_id[i].Item2].first_shift)
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length + 1,
                        rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length + 1,
                        rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift + j/2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length + 1,
                        rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift - j/2 - 1] = 0;
                    }
                }
                else
                {
                    maze[rooms[related_rooms_id[i].Item1].first_shift - 1,
                       rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1,
                        rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift + j/2 + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item1].first_shift - 1,
                        rooms[related_rooms_id[i].Item2].width / 2 + rooms[related_rooms_id[i].Item2].second_shift - j/2 - 1] = 0;
                    }
                }
                continue;
            }

            if (rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length >= rooms[related_rooms_id[i].Item2].first_shift &&
                rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length <= rooms[related_rooms_id[i].Item2].first_shift +
                rooms[related_rooms_id[i].Item2].length)
            {
                    if (rooms[related_rooms_id[i].Item1].second_shift < rooms[related_rooms_id[i].Item2].second_shift)
                    {
                        maze[(rooms[related_rooms_id[i].Item1].first_shift +
                        rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                        + rooms[related_rooms_id[i].Item2].first_shift, rooms[related_rooms_id[i].Item1].second_shift
                        + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                        for (int j = 0; j < widthPassage - 1; j += 2)
                        {
                            maze[(rooms[related_rooms_id[i].Item1].first_shift +
                            rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                            + rooms[related_rooms_id[i].Item2].first_shift + j/2 + 1, rooms[related_rooms_id[i].Item1].second_shift
                            + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                            maze[(rooms[related_rooms_id[i].Item1].first_shift +
                            rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                            + rooms[related_rooms_id[i].Item2].first_shift - j/2 - 1, rooms[related_rooms_id[i].Item1].second_shift
                            + rooms[related_rooms_id[i].Item1].width + 1] = 0;
                        }
                    }
                    else
                    {
                        maze[(rooms[related_rooms_id[i].Item1].first_shift +
                        rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                        + rooms[related_rooms_id[i].Item2].first_shift, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                        for (int j = 0; j < widthPassage - 1; j += 2)
                        {
                            maze[(rooms[related_rooms_id[i].Item1].first_shift +
                            rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                            + rooms[related_rooms_id[i].Item2].first_shift + j/2 + 1, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                            maze[(rooms[related_rooms_id[i].Item1].first_shift +
                            rooms[related_rooms_id[i].Item1].length - rooms[related_rooms_id[i].Item2].first_shift) / 2
                            + rooms[related_rooms_id[i].Item2].first_shift - j/2 - 1, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;
                        }
                    }
                continue;
            }

            if ((rooms[related_rooms_id[i].Item1].first_shift >= rooms[related_rooms_id[i].Item2].first_shift) &&
                (rooms[related_rooms_id[i].Item1].first_shift <= rooms[related_rooms_id[i].Item2].first_shift +
                rooms[related_rooms_id[i].Item2].length))
            {
                if (rooms[related_rooms_id[i].Item1].second_shift < rooms[related_rooms_id[i].Item2].second_shift)
                {
                    maze[(rooms[related_rooms_id[i].Item2].first_shift +
                    rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                    + rooms[related_rooms_id[i].Item1].first_shift, rooms[related_rooms_id[i].Item1].second_shift
                    + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[(rooms[related_rooms_id[i].Item2].first_shift +
                        rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                        + rooms[related_rooms_id[i].Item1].first_shift + j/2 + 1, rooms[related_rooms_id[i].Item1].second_shift
                        + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                        maze[(rooms[related_rooms_id[i].Item2].first_shift +
                        rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                        + rooms[related_rooms_id[i].Item1].first_shift - j/2 - 1, rooms[related_rooms_id[i].Item1].second_shift
                        + rooms[related_rooms_id[i].Item1].width + 1] = 0;
                    }
                }
                else
                {
                    maze[(rooms[related_rooms_id[i].Item2].first_shift +
                    rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                    + rooms[related_rooms_id[i].Item1].first_shift, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[(rooms[related_rooms_id[i].Item2].first_shift +
                        rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                        + rooms[related_rooms_id[i].Item1].first_shift + j/2 + 1, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                        maze[(rooms[related_rooms_id[i].Item2].first_shift +
                        rooms[related_rooms_id[i].Item2].length - rooms[related_rooms_id[i].Item1].first_shift) / 2
                        + rooms[related_rooms_id[i].Item1].first_shift - j/2 - 1, rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;
                    }
                }
                continue;
            }

            if (rooms[related_rooms_id[i].Item1].first_shift < rooms[related_rooms_id[i].Item2].first_shift &&
               rooms[related_rooms_id[i].Item1].first_shift + rooms[related_rooms_id[i].Item1].length >
               rooms[related_rooms_id[i].Item2].first_shift + rooms[related_rooms_id[i].Item2].length)
            {
                if (rooms[related_rooms_id[i].Item1].second_shift < rooms[related_rooms_id[i].Item2].second_shift)
                {
                    maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift,
                        rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift + j/2 + 1,
                        rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width + 1] = 0;

                        maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift - j/2 -1,
                        rooms[related_rooms_id[i].Item1].second_shift + rooms[related_rooms_id[i].Item1].width + 1] = 0;
                    }
                }
                else
                {
                    maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift,
                        rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                    for (int j = 0; j < widthPassage - 1; j += 2)
                    {
                        maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift + j / 2 + 1,
                        rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;

                        maze[rooms[related_rooms_id[i].Item2].length / 2 + rooms[related_rooms_id[i].Item2].first_shift - j / 2 - 1,
                        rooms[related_rooms_id[i].Item1].second_shift - 1] = 0;
                    }
                }
                continue;
            }

            Debug.LogError(related_rooms_id[i].Item1 + " " + related_rooms_id[i].Item2);
            Debug.LogError(rooms[related_rooms_id[i].Item1].first_shift + " , " + rooms[related_rooms_id[i].Item1].second_shift + " , " + rooms[related_rooms_id[i].Item1].length + " , " + rooms[related_rooms_id[i].Item1].width);
            Debug.LogError(rooms[related_rooms_id[i].Item2].first_shift + " , " + rooms[related_rooms_id[i].Item2].second_shift + " , " + rooms[related_rooms_id[i].Item2].length + " , " + rooms[related_rooms_id[i].Item2].width);
        }

        return maze;
    }
}
